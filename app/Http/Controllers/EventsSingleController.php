<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\View;

class EventsSingleController extends Controller
{
    public function index($eventId = 1) {
        if(View::exists('events-single')) {
            if(empty(Post::find($eventId))) {
                abort(404);
            }
            $event = Post::find($eventId);
            $first_event_id = Post::orderBy('id', 'DESC')->first()->id;
            $last_event_id = Post::orderBy('id', 'ASC')->first()->id;

            //Preparing images for slider
            $event->slider_images = trim($event->slider_images, '[]"');
            $event->slider_images = explode('","', $event->slider_images);

            if($event != null) {
                return view('events-single', ['event' => $event, 'first_event_id' => $first_event_id, 'last_event_id' => $last_event_id]);
            }
        }
        abort(404);
    }
}
