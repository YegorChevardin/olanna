<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\View;

class EventsController extends Controller
{
    public function index() {
        if(View::exists('events')) {
            $events = Post::orderBy('id', 'DESC')->where('status', 'published')->paginate(6);

            return view('events', ['events' => $events]);
        }
        abort(404);
    }
}
