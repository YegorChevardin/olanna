<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AppointmentClient;
use Illuminate\Support\Facades\View;

class AppointmentController extends Controller
{
    public function index(Request $request) {
        if(!empty($request)) {
            if(View::exists('tnx')) {
                //Validation
                $data = $request->validate([
                    'name' => 'required|max:50',
                    'telephone' => 'required|max:15',
                    'email' => 'max:30',
                ]);

                //inserting data into database
                $client = new AppointmentClient();
                $client->name = $request->input('name');
                $client->phone = $request->input('telephone');
                $client->email = $request->input('email');
                $client->save();

                return view('tnx');
            }
        }
        abort(404);
    }
}
