<?php

namespace App\Http\Controllers;

use App\Models\Presentation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PresentationsSingleController extends Controller
{
    public function index($presentationId = 1) {
        if(View::exists('presentations-single')) {
            //Debuging of loading process
            if(empty(Presentation::find($presentationId))) {
                abort(404);
            }
            $presentation = Presentation::find($presentationId);
            $first_presentation_id = Presentation::orderBy('id', 'DESC')->first()->id;
            $last_presentation_id = Presentation::orderBy('id', 'ASC')->first()->id;

            //Preparing images for slider
            $presentation->slider_images = trim($presentation->slider_images, '[]"');
            $presentation->slider_images = explode('","', $presentation->slider_images);

            if($presentation != null) {
                return view('presentations-single', ['presentation' => $presentation, 'first_presentation_id' => $first_presentation_id, 'last_presentation_id' => $last_presentation_id]);
            }
        }
        abort(404);
    }
}
