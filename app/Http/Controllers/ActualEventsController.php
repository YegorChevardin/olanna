<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\ActualEventsClient;

class ActualEventsController extends Controller
{
    public function index(Request $request) {
        if(!empty($request)) {
            if(View::exists('tnx')) {
                //Validation
                $data = $request->validate([
                    'name' => 'required|max:50',
                    'telephone' => 'required|max:15',
                    'email' => 'max:30',
                ]);

                //inserting data into database
                $client = new ActualEventsClient();
                $client->name = $request->input('name');
                $client->phone = $request->input('telephone');
                $client->email = $request->input('email');
                $client->save();

                return view('tnx');
            }
        }
        abort(404);
    }
}
