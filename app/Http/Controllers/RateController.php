<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\RateClient;

class RateController extends Controller
{
    public function index(Request $request) {
        if(!empty($request)) {
            if(View::exists('tnx')) {
                //Validation
                $data = $request->validate([
                    'name' => 'required|max:50',
                    'telephone' => 'required|max:15',
                    'feedback' => 'required|max:520',
                ]);

                //inserting data into database
                $client = new RateClient();
                $client->name = $request->input('name');
                $client->number = $request->input('telephone');
                $client->feedback = $request->input('feedback');
                $client->save();

                return view('tnx');
            }
        }
        abort(404);
    }
}
