<?php

namespace App\Http\Controllers;

use App\Models\Presentation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PresentationsController extends Controller
{
    public function index() {
        if(View::exists('presentations')) {
            $presentations = Presentation::orderBy('id', 'DESC')->paginate(6);

            return view('presentations', ['presentations' => $presentations]);
        }
        abort(404);
    }
}
