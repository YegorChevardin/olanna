<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\ActualEvent;
use App\Models\Picture;
use App\Models\Testimonial;
use App\Models\Presentation;

class HomeController extends Controller
{
    public function index() {
        if(View::exists('home')) {
            $actual_event = ActualEvent::orderBy('id', 'ASC')->where('status', 'published')->first();
            $pictures = Picture::orderBy('id', 'DESC')->paginate(6);
            $presentations = Presentation::orderBy('id', 'DESC')->take(3)->get();

            return \view('home', ['actual_event' => $actual_event, 'pictures' => $pictures, 'presentations' => $presentations]);
        }
        abort(404);
    }
}
