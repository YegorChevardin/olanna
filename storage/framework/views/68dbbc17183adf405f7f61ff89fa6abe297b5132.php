<?php $__env->startSection('content'); ?>
    <!-- popup section start -->
    <div id="appointment-popup" class="position-relative mfp-hide mx-auto col-lg-5 bg-warning text-white border-radius-16 shadow-lg p-5 m-5 text-center">
        <h2 class="mb-4 text-dark">Запись на приём</h2>
        <div class="form-container">
            <form id="appointment-form" action="<?php echo e(route('appointment')); ?>" method="post" autocomplete="on">
                <?php echo csrf_field(); ?>
                <div class="mb-3">
                    <input type="text" placeholder="Имя" class="form-control" name="name" required/>
                </div>
                <div class="mb-3">
                    <input type="tel" placeholder="Телефон" class="form-control" name="telephone" required/>
                </div>
                <div class="mb-3">
                    <input type="email" placeholder="Почта" class="form-control" name="email"/>
                </div>
                <button type="submit" class="btn btn-primary btn-lg w-100">Записаться</button>
            </form>
        </div>
    </div>
    <!-- popup section end -->
    <!-- Header section start -->
    <header>
        <div class="container-xl">
            <div class="border-radius-16 overflow-hidden wow bounceInUp">
                <div class="row">
                    <div class="col-lg-5 p-0 m-0 bg-warning d-flex flex-column align-items-center justify-content-center text-center">
                        <div id="header-about">
                            <h1 class="text-light mb-0">
                                Оланна
                            </h1>
                            <h4 class="mb-4">
                                сенсорно - развивающий центр
                            </h4>
                            <ul class="text-start w-auto ms-3 mb-5">
                                <li class="mb-1">Сенсорная интеграция</li>
                                <li class="mb-1">Здоровье</li>
                                <li>Взаимопомощь</li>
                            </ul>
                            <button id="appointment-button" type="button" class="btn btn-light btn-lg mt-5 mb-3">Записаться на приём</button>
                        </div>
                    </div>
                    <div class="col-lg-7 p-0 m-0">
                        <img src="<?php echo e(asset('assets/css/images/banners/header-banner.png')); ?>" class="img-fluid w-100" alt="Olanna center"/>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header section end -->
    <!-- Main content section start -->
    <main>
        <!-- Offer section start -->
        <?php if(isset($actual_event)): ?>
            <section id="offer">
                <div class="container">
                    <div class="bg-white border-radius-16 p-5 wow bounceInUp">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-lg-7">
                                <div id="offer-info">
                                    <h2 class="text-black mb-3"><?php echo e($actual_event->title); ?></h2>
                                    <p>
                                        <?php
                                            echo $actual_event->description;
                                        ?>
                                    </p>
                                    <?php if(isset($actual_event->adress)): ?>
                                        <p><?php echo e($actual_event->adress); ?></p>
                                    <?php endif; ?>
                                    <p>
                                        <span class="text-primary copy text-nowrap">+38 050 342-77-05</span>, <span class="text-primary copy text-nowrap">+38 095 907-53-45</span>, <span class="text-primary copy text-nowrap">+38 093 026-34-06</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div id="offer-form-container" class="border-radius-16 bg-warning wow bounceIn" data-wow-delay="0.8s">
                                    <h5 class="text-dark text-center">Запись</h5>
                                    <form action="<?php echo e(route('actualevent')); ?>" method="post" id="offer-form" autocomplete="on">
                                        <?php echo csrf_field(); ?>
                                        <div class="mb-3">
                                            <input type="text" placeholder="Имя" class="form-control" name="name" required/>
                                        </div>
                                        <div class="mb-3">
                                            <input type="tel" placeholder="Телефон" class="form-control" name="telephone" required/>
                                        </div>
                                        <div class="mb-3">
                                            <input type="email" placeholder="Почта" class="form-control" name="email"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-lg w-100">Записаться</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
        <!-- Offer section end -->
        <!-- Target audience section start -->
        <section id="target-audience" class="text-dark">
            <div class="container">
                <h2 class="mb-5 wow fadeInRight">Для кого</h2>
                <div class="row justify-content-around wow fadeInUp">
                    <div class="col-md-3 align-self-start mb-5">
                        <h1 class="number text-primary p-0 m-0">01</h1>
                        <p class="fw-bold mb-1">Дети</p>
                        <p>С любым нарушением мозга</p>
                    </div>
                    <div class="col-md-2 align-self-end wow fadeInUp" data-wow-delay="0.3s">
                        <h1 class="number text-primary p-0 m-0">02</h1>
                        <p class="fw-bold mb-1">Взрослые</p>
                        <p>Психолог</p>
                    </div>
                    <div class="col-md-2 align-self-start mb-5 wow fadeInUp" data-wow-delay="0.6s">
                        <h1 class="number text-primary p-0 m-0">03</h1>
                        <p class="fw-bold mb-1">Семьи</p>
                        <p>С любым <br/> нарушением мозга</p>
                    </div>
                    <div class="col-md-3 align-self-end wow fadeInUp" data-wow-delay="0.9s">
                        <h1 class="number text-primary p-0 m-0">04</h1>
                        <p class="fw-bold mb-1">Военные</p>
                        <p>С любым нарушением мозга</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Target audience section end -->
        <!-- Education system section start -->
        <section id="education-system">
            <div class="container">
                <h2 class="text-dark mb-5 wow bounceInLeft">Как проходит обучение</h2>
                <div id="timeline">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main-timeline">
                                <div class="timeline">
                                    <div class="timeline-content border-radius-16 bg-warning pt-3 pb-3 ps-4 pe-4 m-5 mb-3 wow bounceInRight" data-wow-delay="0.3s">
                                        <h5 class="text-light mb-1">Развитие сенсорных систем</h5>
                                        <p class="description">
                                            Обанятельная, зрительная, слуховая, вкусовая, тактильная, вестибулярная, проприоцептивная
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline">
                                    <div class="timeline-content border-radius-16 bg-warning pt-3 pb-3 ps-4 pe-4 m-5 wow bounceInLeft" data-wow-delay="0.6s">
                                        <h5 class="text-light mb-1">Сенсомоторное развитие</h5>
                                        <p class="description">
                                            Схема тела, выработка рефлексов, моторное планирование, ориентация сторонам тела, равновесие, восприятие сенсорной информации
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline">
                                    <div class="timeline-content border-radius-16 bg-warning pt-3 pb-3 ps-4 pe-4 m-5 wow bounceInRight" data-wow-delay="0.9s">
                                        <h5 class="text-light mb-1">Перцептивно-моторное развитие</h5>
                                        <p class="description">
                                            Координация  глаз-рука, контроль за положением тела, глазо-двигательный контроль, слухо-речевые навыки, визуально-пространственное восприятие
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline">
                                    <div class="timeline-content border-radius-16 bg-warning pt-3 pb-3 ps-4 pe-4 m-5 wow bounceInLeft" data-wow-delay="1.2s">
                                        <h5 class="text-light mb-1">Познавательные способности</h5>
                                        <p class="description">
                                            Академическое обучение, повседневная жизнедеятельность, поведение
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Education system section end -->
        <!-- Gallery section start -->
        <?php if(isset($pictures[0])): ?>
            <section id="gallery">
                <div class="container">
                    <h2 class="text-dark mb-5 wow bounceInLeft">Галерея</h2>
                    <div class="row" data-masonry='{"percentPosition": true }'>
                        <?php $__empty_1 = true; $__currentLoopData = $pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <div class="col-sm-12 col-lg-6">
                                <div class="card border-0 border-radius-16 wow bounceInUp <?php if($picture != $pictures[0] and $picture != $pictures[1]): ?> mt-4 <?php elseif($picture == $pictures[1]): ?> mt-5 <?php else: ?> <?php endif; ?>" data-wow-delay="0.3s">
                                    <div class="card-image-container">
                                        <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$picture->image)); ?>" data-mfp-src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$picture->image)); ?>" class="img-fluid w-100" alt="<?php echo e($picture->title); ?>"/>
                                    </div>
                                    <div class="card-body bg-warning">
                                        <p class="card-text text-light text-center"><?php echo e($picture->title); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                    </div>
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-12 text-center">
                            <div class="d-flex justify-content-center align-items-center">
                                <?php echo e($pictures->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php else: ?>
            <section id="gallery">
                <div class="container">
                    <div class="row justify-content-center align-itmes-center">
                        <div class="col-md-6 text-center">
                            <h3 class="wow bounceInUp">
                                Увы пока гелерея не заполнена
                            </h3>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
        <!-- Gallery section end -->
        <!-- Prices section start -->
        <section id="prices">
            <div class="container">
                <h2 class="text-dark mb-5 wow bounceInLeft">Цены на услуги</h2>
                <div class="table-container bg-warning border-radius-16 text-black p-4 ps-5 pe-5 wow bounceInUp">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless">
                            <thead>
                            <tr class="text-light">
                                <th>Категория</th>
                                <th class="border-0"></th>
                                <th class="text-center">Время</th>
                                <th class="border-0"></th>
                                <th class="text-center">Цена, ₴</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Логоритмика</td>
                                <td class="border-0"></td>
                                <td>30 мин - 1 час</td>
                                <td class="border-0"></td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>Массаж</td>
                                <td class="border-0"></td>
                                <td>30 мин - 2:30 ч</td>
                                <td class="border-0"></td>
                                <td>500 - 30</td>
                            </tr>
                            <tr>
                                <td>Йога</td>
                                <td class="border-0"></td>
                                <td>1 час</td>
                                <td class="border-0"></td>
                                <td>80</td>
                            </tr>
                            <tr>
                                <td>Оздоровительная физкультура</td>
                                <td class="border-0"></td>
                                <td>30 мин</td>
                                <td class="border-0"></td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>Зал сенсорной интеграции</td>
                                <td class="border-0"></td>
                                <td>1 час</td>
                                <td class="border-0"></td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>Индивидуальные консультации логопеда</td>
                                <td class="border-0"></td>
                                <td>30 мин</td>
                                <td class="border-0"></td>
                                <td>100</td>
                            </tr>
                            <tr>
                                <td>Общее развитие ребёнка</td>
                                <td class="border-0"></td>
                                <td>1 час</td>
                                <td class="border-0"></td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>Занятия методом восточных оздоровительных систем</td>
                                <td class="border-0"></td>
                                <td>1 час</td>
                                <td class="border-0"></td>
                                <td>50</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <!-- Prices section end -->
        <!-- Main page presentations section start -->
        <section id="main-page-presentations">
            <div class="container">
                <h2 class="text-dark mb-5 wow bounceInLeft">Проект по обмену опытом с ЕС</h2>
                <div class="row" data-masonry='{"percentPosition": true }'>
                    <?php $__empty_1 = true; $__currentLoopData = $presentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="col-sm-12 col-lg-4 mt-4">
                            <div class="card border-0 border-radius-16 wow bounceInUp" data-wow-delay="0.3s" onclick="window.location='presentations-single/<?php echo e($presentation->id); ?>'">
                                <?php if(isset($presentation->image)): ?>
                                    <div class="card-image-container">
                                        <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$presentation->image)); ?>" class="img-fluid w-100" alt="Событие <?php echo e($presentation->id); ?>"/>
                                    </div>
                                <?php else: ?>
                                    <div class="card-image-container bg-primary p-2"></div>
                                <?php endif; ?>
                                <div class="card-body bg-primary text-light p-4 pb-5">
                                    <h5 class="card-title"><?php echo e($presentation->title); ?></h5>
                                    <p class="card-text">
                                        <?php echo e($presentation->excerpt); ?>

                                    </p>
                                    <p class="card-text text-white fw-bold"><time datetime="2020-01-01"><?php echo e(date('M d, Y', strtotime($presentation->created_at))); ?></time></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div class="col-md-12 align-self-center text-end">
                            <h2 class="text-dark m-5 wow bounceInRight" data-wow-delay="0.2s">Увы, пока у нас не было никаких зафиксированных презентаций.</h2>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if(!empty($presentations[0])): ?>
                    <div class="row justify-content-center align-items-center mt-5">
                        <div class="col-md-12 text-center">
                            <div class="d-flex justify-content-center align-items-center">
                                <a target="_blank" href="<?php echo e(route('presentations')); ?>" class="btn btn-primary btn-lg wow bounceIn">Посмотреть больше презентаций</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
        <!-- Main page presentations section end -->
        <!-- Contacts section start -->
        <section id="contacts">
            <div class="container">
                <h2 class="text-dark mb-5 wow bounceInRight">Контакты</h2>
            </div>
            <div class="container position-relative wow bounceInUp">
                <div class="height-adjuster">
                    <div id="contacts-map" class="w-100 h-100 ratio overflow-hidden border-radius-16 position-absolute">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2692.6470460124715!2d34.4025900151509!3d47.55519779952146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dca3631f39cb85%3A0x89183fbd5b76c04!2z0YPQuy4g0JDRgtCw0LzQsNC90YHQutCw0Y8sIDYsINCd0LjQutC-0L_QvtC70YwsINCU0L3QtdC_0YDQvtC_0LXRgtGA0L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA1MzIwMA!5e0!3m2!1sru!2sua!4v1629216498497!5m2!1sru!2sua" allowfullscreen loading="lazy"></iframe>
                    </div>
                    <div id="contacts-info" class="contacts-container bg-warning border-radius-16 position-absolute p-4 m-5">
                        <h5 class="text-dark">Свяжитесь с нами удобным для вас способом:</h5>
                        <div class="contacts">
                            <ul class="m-0 p-0">
                                <li class="mb-3 copy ps-4">
                                    г. Никополь. ул. Отаманская 6
                                </li>
                                <li class="copy ps-4">
                                    +380 930 263 406
                                </li>
                            </ul>
                        </div>
                        <p class="text-black mt-3">Или закажите звонок:</p>
                        <div class="contacts-form-container">
                            <form id="contact-form" action="<?php echo e(route('rateus')); ?>" method="post" autocomplete="on">
                                <?php echo csrf_field(); ?>
                                <div class="mb-3">
                                    <input type="text" placeholder="Имя" class="form-control" name="name" required/>
                                </div>
                                <div class="mb-3">
                                    <input type="tel" placeholder="Телефон" class="form-control" name="telephone" required/>
                                </div>
                                <div class="mb-4">
                                    <textarea placeholder="Опишите ваш вопрос" class="form-control" name="feedback" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg w-100">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contacts section end -->
    </main>
    <!-- Main content section end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/olanna/resources/views/home.blade.php ENDPATH**/ ?>