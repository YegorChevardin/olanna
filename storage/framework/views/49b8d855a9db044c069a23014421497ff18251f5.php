<?php $__env->startSection('content'); ?>
    <!-- Main section start -->
    <main id="events">
        <div class="container">
            <h2 class="text-dark wow bounceInLeft">События</h2>
            <div class="row" data-masonry='{"percentPosition": true }'>
                <?php $__empty_1 = true; $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="col-sm-12 col-lg-4 mt-4">
                        <div class="card border-0 border-radius-16 wow bounceInUp" data-wow-delay="0.3s" onclick="window.location='events-single/<?php echo e($event->id); ?>'">
                            <?php if(isset($event->image)): ?>
                                <div class="card-image-container">
                                    <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$event->image)); ?>" class="img-fluid w-100" alt="Событие <?php echo e($event->id); ?>"/>
                                </div>
                            <?php else: ?>
                                <div class="card-image-container bg-warning p-2"></div>
                            <?php endif; ?>
                            <div class="card-body bg-warning text-dark p-4 pb-5">
                                <h5 class="card-title"><?php echo e($event->title); ?></h5>
                                <p class="card-text">
                                    <?php echo e($event->excerpt); ?>

                                </p>
                                <p class="card-text text-white"><time datetime="2020-01-01"><?php echo e(date('M d, Y', strtotime($event->created_at))); ?></time></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-md-12 align-self-center text-end">
                        <h2 class="text-dark m-5 wow bounceInRight" data-wow-delay="0.2s">Увы, пока у нас не было никаких зафиксированных событий.</h2>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row justify-content-center align-items-center mt-5">
                <div class="col-md-12 text-center">
                    <div class="d-flex justify-content-center align-items-center">
                        <?php echo e($events->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/olanna/resources/views/events.blade.php ENDPATH**/ ?>