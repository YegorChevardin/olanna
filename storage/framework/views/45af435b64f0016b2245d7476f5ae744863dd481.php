<nav id="scroll-nav" class="navbar navbar-expand-lg navbar-light bg-light fixed-top shadow-lg">
    <div class="container p-0">
        <a class="navbar-brand d-flex align-items-center justify-content-center" href="<?php echo e(route('home')); ?>">
            <img src="<?php echo e(asset('assets/css/images/logo.png')); ?>" alt="logo" style="height: 42px;" class="d-inline-block align-text-top me-3">
            <strong>Оланна</strong>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#menuContent" aria-controls="menuContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="menuContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <?php if(\Route::current()->getName() == 'home'): ?>
                    <li class="nav-item me-4">
                        <a class="nav-link" aria-current="page" href="https://olanna.bitrix24site.ua/" target="_blank"><span class="text-dark">Центр "Оланна"</span></a>
                    </li>
                    <li class="nav-item me-4">
                        <a class="nav-link" href="<?php echo e(route('presentations')); ?>" target="_blank"><span class="text-dark">Обмен опыта с ЕС</span></a>
                    </li>
                    <li class="nav-item me-4">
                        <a class="nav-link" href="#contacts"><span class="text-dark">Контакты</span></a>
                    </li>
                <?php else: ?>
                    <li class="nav-item me-4">
                        <a class="nav-link" aria-current="page" href="https://olanna.bitrix24site.ua/" target="_blank"><span class="text-dark">Центр "Оланна"</span></a>
                    </li>
                    <li class="nav-item me-4">
                        <a class="nav-link" href="<?php echo e(route('presentations')); ?>" target="_blank"><span class="text-dark">Обмен опыта с ЕС</span></a>
                    </li>
                    <li class="nav-item me-4">
                        <a class="nav-link" href="<?php echo e(route('home')); ?>#contacts"><span class="text-dark">Контакты</span></a>
                    </li>
                <?php endif; ?>
                <?php if(auth()->guard()->guest()): ?>
                    <li class="nav-item dropdown me-4">
                        <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" role="button" aria-expanded="false"><span class="text-primary">Войти</span></a>
                        <ul class="dropdown-menu">
                            <?php if(Route::has('login')): ?>
                                <li><a class="dropdown-item" href="<?php echo e(route('login')); ?>">Войти</a></li>
                            <?php endif; ?>
                            <li><a class="dropdown-item" href="<?php echo e(route('register')); ?>">Зарегистрироваться</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li class="nav-item dropdown me-4">
                        <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo e(Auth::user()->name); ?>

                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выйти</a></li>
                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                <?php echo csrf_field(); ?>
                            </form>
                            <?php if(Auth::user()->hasRole('admin')): ?>
                                <li><a class="dropdown-item" href="<?php echo e(route('voyager.dashboard')); ?>">Админ панель</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><span class="text-dark">Ещё</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="<?php echo e(route('events')); ?>">События</a></li>
                        <li><a class="dropdown-item" href="http://eco-csr.save-eco.world/" target="_blank">Каталог бизнеса</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php /**PATH /opt/lampp/htdocs/olanna/resources/views/layouts/scrol-nav-bar.blade.php ENDPATH**/ ?>