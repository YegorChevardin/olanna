<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>
            <?php if(\Route::current()->getName() == 'presentations'): ?>
                Проект по обмену опытом с ЕС
            <?php else: ?>
                Сенсорно-развивающий центр "Оланна"
            <?php endif; ?>
        </title>
        <!-- Styles section start -->
        <?php echo $__env->make('layouts.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Styles section end -->
    </head>
    <body>
        <!-- Back to top button section start -->
        <a id="back-top-button" class="p-2 shadow-sm"></a>
        <!-- Back to top button section end -->
        <!-- Scrolling navbar section start -->
        <?php echo $__env->make('layouts.scrol-nav-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Scrolling navbar section end -->
        <!-- Navigation bar section start -->
        <?php echo $__env->make('layouts.nav-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Navigation bar section end -->
        <!-- Errors section start -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-12 text-center">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger shadow-lg border-radius-16 wow bounceIn">
                            <ul style="list-style-type: none;">
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- Errors section end -->
        <!-- Content section start -->
        <?php echo $__env->yieldContent('content'); ?>
        <!-- Content section end -->
        <!-- Footer section start -->
        <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Footer section end -->
        <!-- Scripts section start -->
        <?php echo $__env->make('layouts.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Scripts section end -->
    </body>
</html>
<?php /**PATH /opt/lampp/htdocs/olanna/resources/views/layouts/template.blade.php ENDPATH**/ ?>