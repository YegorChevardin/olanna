<?php $__env->startSection('content'); ?>
    <!-- Main section start -->
    <main id="presentations">
        <div class="container">
            <h2 class="text-primary wow bounceInLeft">Проект по обмену опытом с ЕС</h2>
            <p class="text-black wow bounceInLeft" data-wow-delay="0.2s">
                Обмен опытом с организациями из ЕС по масштабированию социального бизнеса и усилению влияния социальных предприятий. Трехстороннее сотрудничество: общественные организации – социальный бизнес – власть
            </p>
            <div class="row" data-masonry='{"percentPosition": true }'>
                <?php $__empty_1 = true; $__currentLoopData = $presentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="col-sm-12 col-lg-4 mt-4">
                        <div class="card border-0 border-radius-16 wow bounceInUp" data-wow-delay="0.3s" onclick="window.location='presentations-single/<?php echo e($presentation->id); ?>'">
                            <?php if(isset($presentation->image)): ?>
                                <div class="card-image-container">
                                    <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$presentation->image)); ?>" class="img-fluid w-100" alt="Событие <?php echo e($presentation->id); ?>"/>
                                </div>
                            <?php else: ?>
                                <div class="card-image-container bg-primary p-2"></div>
                            <?php endif; ?>
                            <div class="card-body bg-primary text-light p-4 pb-5">
                                <h5 class="card-title"><?php echo e($presentation->title); ?></h5>
                                <p class="card-text">
                                    <?php echo e($presentation->excerpt); ?>

                                </p>
                                <p class="card-text text-white fw-bold"><time datetime="2020-01-01"><?php echo e(date('M d, Y', strtotime($presentation->created_at))); ?></time></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-md-12 align-self-center text-end">
                        <h2 class="text-dark m-5 wow bounceInRight" data-wow-delay="0.2s">Увы, пока у нас не было никаких зафиксированных проектов.</h2>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row justify-content-center align-items-center mt-5">
                <div class="col-md-12 text-center">
                    <div class="d-flex justify-content-center align-items-center">
                        <?php echo e($presentations->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
    <!-- Partners section start -->
    <section id="partners" class="mb-5">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 text-center mb-1">
                    <img src="<?php echo e(asset('assets/css/images/partners_logos.jpg')); ?>" alt="Partners"/>
                </div>
            </div>
            <p class="text-center mt-4">
                Проект финансируется Европейским Союзом в рамках программы House of Europe.
            </p>
        </div>
    </section>
    <!-- Partners section end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/olanna/resources/views/presentations.blade.php ENDPATH**/ ?>