<?php $__env->startSection('content'); ?>
    <!-- Main section start -->
    <main id="presentation">
        <div class="container">
            <?php if(isset($presentation->image)): ?>
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-9">
                        <div class="ratio ratio-21x9 overflow-hidden wow bounceInUp">
                            <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$presentation->image)); ?>" class="img-fluid" alt="Событие <?php echo e($presentation->id); ?>"/>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="info mb-5">
                        <p class="text-black date wow bounceInLeft" data-wow-delay="0.1s"><time datetime="2020-01-01"><?php echo e(date('M d, Y', strtotime($presentation->created_at))); ?></time></p>
                        <h2 class="text-dark wow bounceInLeft" data-wow-delay="0.2s">
                            <?php echo e($presentation->title); ?>

                        </h2>
                        <p class="text-dark place mt-4 wow bounceInLeft" data-wow-delay="0.3s"><?php echo e($presentation->excerpt); ?></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center">
                <div class="col-md-9">
                    <div class="content text-dark mb-5">
                        <?php if(empty($presentation->slider_images[0])): ?>
                        <?php else: ?>
                            <div class="img-slider wow bounceInUp" data-wow-delay="0.5s">
                                <?php $__currentLoopData = $presentation->slider_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider_image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="ratio ratio-16x9 border-radius-16 overflow-hidden">
                                        <img src="<?php echo e(asset('storage'.DIRECTORY_SEPARATOR.$slider_image)); ?>" class="img-fluid" alt="Событие <?php echo e($presentation->id); ?>"/>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php endif; ?>
                        <p class="wow bounceInLeft" data-wow-delay="0.6s">
                            <?php echo $presentation->body;?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center justify-content-between">
                        <?php if($presentation->id == $first_presentation_id): ?>
                            <p class="text-dark wow bounceInLeft">Нет новых постов</p>
                        <?php else: ?>
                            <a class="wow bounceInRight" href="/presentations-single/<?php echo e($presentation->id + 1); ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                                </svg>
                                Предыдущая статья
                            </a>
                        <?php endif; ?>
                        <?php if($presentation->id == $last_presentation_id): ?>
                            <p class="text-dark wow bounceInLeft">Конец</p>
                        <?php else: ?>
                            <a class="wow bounceInLeft" href="/presentations-single/<?php echo e($presentation->id - 1); ?>">
                                Следующая статья
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/olanna/resources/views/presentations-single.blade.php ENDPATH**/ ?>