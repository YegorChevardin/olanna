<footer class="text-dark bg-light pt-5 pb-5">
    <div class="container">
        <div class="row align-items-start">
            <div id="footer-img" class="col-lg-2 mb-3">
                <img class="img-fluid" src="<?php echo e(asset('assets/css/images/logo.png')); ?>" alt="logo"/>
            </div>
            <div class="col-lg-3 offset-1 mb-3">
                <div class="text-container">
                    <p>Пн - Вс 08:00 - 20:00</p>
                    <p><span class="copy">ollanacentre@gmail.com</span></p>
                    <p><span class="copy">+38 093 026-34-06</span></p>
                    <p><span class="copy">+38 095 907-53-45</span></p>
                </div>
            </div>
            <div class="col-lg-2 offset-1 mb-3">
                <p><a class="text-dark" href="<?php echo e(route('events')); ?>">События</a></p>
                <p><a class="text-dark" href="<?php echo e(route('presentations')); ?>" target="_blank">Презентации</a></p>
                <p><a class="text-dark" href="http://eco-csr.save-eco.world/" target="_blank">Каталог бизнеса</a></p>
            </div>
            <div class="col-lg-1">
                <div id="social-links" class="d-flex justify-content-center align-items-center">
                    <img class="me-3" width="44" height="44" src="<?php echo e(asset('assets/css/images/svg/InstagramLogo.svg')); ?>" alt="instagram" onclick="window.location='https://www.instagram.com/olanna_nikopol/'"/>
                    <img width="44" height="44" src="<?php echo e(asset('assets/css/images/svg/FacebookLogo.svg')); ?>" alt="facebook" onclick="window.location='https://www.facebook.com/profile.php?id=100052336176818'"/>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php /**PATH /opt/lampp/htdocs/olanna/resources/views/layouts/footer.blade.php ENDPATH**/ ?>