<section id="static-menu">
    <div class="w-100 mt-4 pb-3">
        <div class="container-xl">
            <div class="d-flex align-items-start justify-content-between">
                <a class="navbar-brand w-25 p-0 me-0" href="<?php echo e(route('home')); ?>">
                    <img src="<?php echo e(asset('assets/css/images/logo.png')); ?>" alt="logo"/>
                </a>
                <div id="static-menu-items" class="w-75">
                    <ul id="static-menu-info" class="nav d-flex bd-highlight justify-content-between align-items-center">
                        <li class="nav-item flex-fill text-start">
                            <p class="mb-1">
                                <span class="fs-14">Пн - Вс</span>
                                <br/>
                                <span class="text-primary">с 08:00 до 20:00</span>
                            </p>
                        </li>
                        <li class="nav-item border-deviders flex-fill justify-content-center d-flex ps-5 pe-5">
                            <p class="mb-1 text-start">
                                <span class="fs-14">Почта</span>
                                <br/>
                                <span class="text-primary copy">ollanacentre@gmail.com</span>
                            </p>
                        </li>
                        <li class="nav-item flex-fill text-end">
                            <p class="mb-1">
                                <span class="fs-14">Обратный звонок</span>
                                <br/>
                                <span class="text-primary copy">+38 093 026-34-06</span>
                            </p>
                        </li>
                    </ul>
                    <nav class="navbar navbar-light navbar-lower navbar-expand-lg p-0">
                        <div class="navbar-header">
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#staticMenuContent" aria-controls="staticMenuContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="staticMenuContent">
                            <ul class="nav w-100 justify-content-between align-items-end">
                                <?php if(\Route::current()->getName() == 'home'): ?>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" aria-current="page" href="https://olanna.bitrix24site.ua/" target="_blank"><span class="text-dark">Центр "Оланна"</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" href="<?php echo e(route('presentations')); ?>" target="_blank"><span class="text-dark">Обмен опыта с ЕС</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" href="#contacts"><span class="text-dark">Контакты</span></a>
                                    </li>
                                <?php else: ?>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" aria-current="page" href="https://olanna.bitrix24site.ua/" target="_blank"><span class="text-dark">Центр "Оланна"</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" href="<?php echo e(route('presentations')); ?>" target="_blank"><span class="text-dark">Обмен опыта с ЕС</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pt-0" href="<?php echo e(route('home')); ?>/#contacts"><span class="text-dark">Контакты</span></a>
                                    </li>
                                <?php endif; ?>
                                <?php if(auth()->guard()->guest()): ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><span class="text-primary">Войти</span></a>
                                        <ul class="dropdown-menu">
                                            <?php if(Route::has('login')): ?>
                                                <li><a class="dropdown-item" href="<?php echo e(route('login')); ?>">Войти</a></li>
                                            <?php endif; ?>
                                            <li><a class="dropdown-item" href="<?php echo e(route('register')); ?>">Зарегистрироваться</a></li>
                                        </ul>
                                    </li>
                                <?php else: ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                                            <?php echo e(Auth::user()->name); ?>

                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выйти</a></li>
                                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                                <?php echo csrf_field(); ?>
                                            </form>
                                            <?php if(Auth::user()->hasRole('admin')): ?>
                                                <li><a class="dropdown-item" href="<?php echo e(route('voyager.dashboard')); ?>">Админ панель</a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-primary" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><span class="text-dark">Ещё</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="<?php echo e(route('events')); ?>">События</a></li>
                                        <li><a class="dropdown-item" href="http://eco-csr.save-eco.world/" target="_blank">Каталог бизнеса</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /**PATH /opt/lampp/htdocs/olanna/resources/views/layouts/nav-bar.blade.php ENDPATH**/ ?>