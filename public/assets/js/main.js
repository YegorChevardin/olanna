$(document).ready(function() {
    //Wow animation activation
    new WOW().init();

    /* Back to top button section start */
    var btn = $('#back-top-button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
    /* Back to top button section end */

    /* Scrolling navbar section start */
    var prevScrollpos = window.pageYOffset;

    window.onscroll = function() {
        var currentScrollpos = window.pageYOffset;

        if(currentScrollpos >= 15 * 16 & $(window).scrollTop() > 500) {
            if(prevScrollpos > currentScrollpos) {
                document.getElementById('scroll-nav').style.top = "0";
            } else {
                document.getElementById('scroll-nav').style.top = "-5.1em";
            }
            prevScrollpos = currentScrollpos;
        } else {
            document.getElementById('scroll-nav').style.top = "-5.1em";
        }
    }
    /* Scrolling navbar section end */

    /* Copy class section start */
    $('.copy').on('click', function () {
        var text = this.innerText;
        var $tmp = $("<input>");
        $("body").append($tmp);
        $tmp.val(text).select();
        document.execCommand("copy");
        $tmp.remove();
        alert("Текст удачно скопирован!\n" + text);
    });
    /* Copy class section end */

    /* Magnific popup section start */
    $('#appointment-button').magnificPopup({
        removalDelay: 300,
        mainClass: 'mfp-fade',
        items: {
            src: '#appointment-popup'
        }
    });
    $('#gallery .card').magnificPopup({
        delegate: 'img',
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });
    /* Magnific popup section end */

    /* Dropdown animation section start */
    // Add slideDown animation to Bootstrap dropdown when expanding.
    $('.dropdown').on('show.bs.dropdown', function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
    /* Dropdown animation section end */

    /* Testimonials carousel section start */
            $('.carousel-items').slick({
                dots: true,
                infinite: true,
                speed: 800,
                autoplay: true,
                autoplaySpeed: 1500,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1047,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 618,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            dots: false
                        }
                    }

                ]
            });
    /* Testimonials carousel section end */

    /* Event carousel section start */
    $('.img-slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoplay: true,
        autoplaySpeed: 1500,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1047,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 618,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: false
                }
            }

        ]
    });
    /* Event carousel section start */
});
