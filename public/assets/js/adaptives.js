//This file contains adaptive functions for website design

if($(window).width() <= 991) {
    //Smaller than 1030 pixels
    $("#static-menu").addClass("d-none");
    $("header").addClass("mt-3");
    $("#contacts-map").removeClass("position-absolute");
    $("#contacts-map").addClass("ratio-16x9");
    $("#contacts-info").removeClass("position-absolute");
    $("#contacts-info").removeClass("m-5");
    $("#contacts-info").addClass("mt-5");
    $(".height-adjuster").addClass("d-flex");
    $(".height-adjuster").addClass("flex-column");
    $("#contacts-info").addClass("align-self-center");
    $("footer .row").removeClass("align-items-start");
    $("footer .row").addClass("justify-content-center");
    $("footer .row").addClass("text-center");
    $("#social-links").removeClass("justify-content-center");
    $("#social-links").addClass("justify-content-between");
    $("#footer-img").addClass("d-none");
}
