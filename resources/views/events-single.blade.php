@extends('layouts.template')
@section('content')
    <!-- Main section start -->
    <main id="event">
        <div class="container">
            @if(isset($event->image))
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-9">
                        <div class="ratio ratio-21x9 overflow-hidden wow bounceInUp">
                            <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$event->image) }}" class="img-fluid" alt="Событие {{ $event->id }}"/>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="info mb-5">
                        <p class="text-black date wow bounceInLeft" data-wow-delay="0.1s"><time datetime="2020-01-01">{{ date('M d, Y', strtotime($event->created_at)) }}</time></p>
                        <h2 class="text-dark wow bounceInLeft" data-wow-delay="0.2s">
                            {{ $event->title }}
                        </h2>
                        <p class="text-dark place mt-4 wow bounceInLeft" data-wow-delay="0.3s">{{ $event->excerpt }}</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center">
                <div class="col-md-9">
                    <div class="content text-dark mb-5">
                        @if(empty($event->slider_images[0]))
                        @else
                            <div class="img-slider wow bounceInUp" data-wow-delay="0.5s">
                                @foreach($event->slider_images as $slider_image)
                                    <div class="ratio ratio-16x9 border-radius-16 overflow-hidden">
                                        <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$slider_image) }}" class="img-fluid" alt="Событие {{ $event->id }}"/>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <p class="wow bounceInLeft" data-wow-delay="0.6s">
                            <?php echo $event->body;?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center justify-content-between">
                        @if($event->id == $first_event_id)
                            <p class="text-dark wow bounceInLeft">Нет новых постов</p>
                        @else
                            <a class="wow bounceInRight" href="/events-single/{{ $event->id + 1 }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                                </svg>
                                Предыдущая статья
                            </a>
                        @endif
                        @if($event->id == $last_event_id)
                            <p class="text-dark wow bounceInLeft">Конец</p>
                        @else
                            <a class="wow bounceInLeft" href="/events-single/{{ $event->id - 1 }}">
                                Следующая статья
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
