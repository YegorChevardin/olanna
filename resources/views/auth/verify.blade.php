@extends('layouts.template')
@section('content')
    <!-- Main section start -->
    <main>
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8">
                    <div class="wow bounceIn">
                        <div class="card">
                            <div class="card-header">Подтвердите свой адрес электронной почты</div>
                            <div class="card-body">
                                @if (session('resent'))
                                    <div class="alert alert-success" role="alert">
                                        Ссылка на подтверждение была отправлена вам в ящик на почту
                                    </div>
                                @endif
                                Прежде чем продолжить, проверьте свою электронную почту на наличие ссылки для подтверждения.
                                Если вы не получили письмо,
                                <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-link p-0 m-0 align-baseline">нажмите здесь, чтобы запросить ещё</button>.
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
