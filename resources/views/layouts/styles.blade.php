<link rel="shortcut icon" href="{{ asset('assets/css/images/favicon.png') }}"/>
<link rel="stylesheet" href="{{ asset('assets/libs/bootstrap/css/bootstrap.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-skins.css') }}"/>
<!-- Magnific popup section start -->
<link rel="stylesheet" href="{{ asset('assets/libs/magnific-popup/magnific-popup.css') }}"/>
<!-- Magnific popup section end -->
<link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}"/>
<!-- Carousel styles section start -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/slick-slider/slick-theme.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/slick-slider/slick-carousel.css') }}"/>
<!-- Carousel styles section end -->
<!-- Wow animation section start -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/wow-animation/animate.css') }}"/>
<!-- Wow animation section end -->
