<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>
            @if(\Route::current()->getName() == 'presentations')
                Проект по обмену опытом с ЕС
            @else
                Сенсорно-развивающий центр "Оланна"
            @endif
        </title>
        <!-- Styles section start -->
        @include('layouts.styles')
        <!-- Styles section end -->
    </head>
    <body>
        <!-- Back to top button section start -->
        <a id="back-top-button" class="p-2 shadow-sm"></a>
        <!-- Back to top button section end -->
        <!-- Scrolling navbar section start -->
        @include('layouts.scrol-nav-bar')
        <!-- Scrolling navbar section end -->
        <!-- Navigation bar section start -->
        @include('layouts.nav-bar')
        <!-- Navigation bar section end -->
        <!-- Errors section start -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-12 text-center">
                    @if($errors->any())
                        <div class="alert alert-danger shadow-lg border-radius-16 wow bounceIn">
                            <ul style="list-style-type: none;">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Errors section end -->
        <!-- Content section start -->
        @yield('content')
        <!-- Content section end -->
        <!-- Footer section start -->
        @include('layouts.footer')
        <!-- Footer section end -->
        <!-- Scripts section start -->
        @include('layouts.scripts')
        <!-- Scripts section end -->
    </body>
</html>
