<script type="text/javascript" src="{{ asset('assets/libs/JQuery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('assets/libs/Masorny.js') }}" defer></script>
<!-- Carousel scripts section start -->
<script type="text/javascript" src="{{ asset('assets/libs/slick-slider/Slick.js') }}" defer></script>
<!-- Carousel scripts section end -->
<!-- Magnific popup section start -->
<script type="text/javascript" src="{{ asset('assets/libs/magnific-popup/jquery.magnific-popup.js') }}" defer></script>
<!-- Magnific popup section end -->
<!-- Wow animation section start -->
<script type="text/javascript" src="{{ asset('assets/libs/wow-animation/wow.min.js') }}" defer></script>
<!-- Wow animation section end -->
<script type="text/javascript" src="{{ asset('assets/js/adaptives.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('assets/js/main.js') }}" defer></script>
