@extends('layouts.template')
@section('content')
    <!-- Main section start -->
    <main>
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-6">
                    <div class="wow bounceIn">
                        <h1 class="text-dark mb-5">Спасибо за заявку!</h1>
                        <p>
                            Наш менеджер перезвонит вам, а пока что можете посмотреть наши мероприятия, кликнув по этой <a href="{{ route('events') }}">ссылке</a>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
