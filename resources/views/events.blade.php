@extends('layouts.template')
@section('content')
    <!-- Main section start -->
    <main id="events">
        <div class="container">
            <h2 class="text-dark wow bounceInLeft">События</h2>
            <div class="row" data-masonry='{"percentPosition": true }'>
                @forelse($events as $event)
                    <div class="col-sm-12 col-lg-4 mt-4">
                        <div class="card border-0 border-radius-16 wow bounceInUp" data-wow-delay="0.3s" onclick="window.location='events-single/{{ $event->id }}'">
                            @if(isset($event->image))
                                <div class="card-image-container">
                                    <img src="{{ asset('storage'.DIRECTORY_SEPARATOR.$event->image) }}" class="img-fluid w-100" alt="Событие {{ $event->id }}"/>
                                </div>
                            @else
                                <div class="card-image-container bg-warning p-2"></div>
                            @endif
                            <div class="card-body bg-warning text-dark p-4 pb-5">
                                <h5 class="card-title">{{ $event->title }}</h5>
                                <p class="card-text">
                                    {{ $event->excerpt }}
                                </p>
                                <p class="card-text text-white"><time datetime="2020-01-01">{{ date('M d, Y', strtotime($event->created_at)) }}</time></p>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12 align-self-center text-end">
                        <h2 class="text-dark m-5 wow bounceInRight" data-wow-delay="0.2s">Увы, пока у нас не было никаких зафиксированных событий.</h2>
                    </div>
                @endforelse
            </div>
            <div class="row justify-content-center align-items-center mt-5">
                <div class="col-md-12 text-center">
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $events->links() }}
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main section end -->
@endsection
