<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\EventsSingleController;
use App\Http\Controllers\PresentationsController;
use App\Http\Controllers\PresentationsSingleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\ActualEventsController;
use App\Http\Controllers\RateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/home', function() {
    return redirect(\route('home'));
});

Route::get('/events',[EventsController::class, 'index'])->name('events');
Route::get('/events-single/{id?}', [EventsSingleController::class, 'index'])->name('events-single');

Route::get('/presentations',[PresentationsController::class, 'index'])->name('presentations');
Route::get('/presentations-single/{id?}', [PresentationsSingleController::class, 'index'])->name('presentations-single');

Route::post('/appointment', [AppointmentController::class, 'index'])->name('appointment');
Route::post('/actualevent', [ActualEventsController::class, 'index'])->name('actualevent');
Route::post('/rateus', [RateController::class, 'index'])->name('rateus');

Auth::routes();

Route::get('logout', function () {
    abort(404);
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
